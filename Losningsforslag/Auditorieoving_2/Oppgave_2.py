# Oppgave 2a)
def is_vocal(chr):
    vocals = 'aeiouyæøå'
    if chr in vocals:
        return True
    return False


# Oppgave 2b)
def number_of_vocals(streng):
    num_vocals = 0
    for ch in streng:
        if is_vocal(ch):
            num_vocals += 1
    return num_vocals


# Oppgave 2c)
# Under er det vist tre forskjellige forslag til hvordan denne deloppgaven kan løses
def number_of_consonants(streng):
    num_consonants = 0
    for ch in streng:
        if not (is_vocal(ch) or ch in '., '):
            num_consonants += 1
    return num_consonants


def number_of_consonants(streng):
    num_consonants = 0
    for ch in streng:
        if not is_vocal(ch) and str.isalpha(ch):
            num_consonants += 1
    return num_consonants


def number_of_consonants(streng):
    return len(streng)-number_of_vocals(streng)-streng.count('.')-streng.count(',')-streng.count(' ')


# print-setningene som er gitt under eksempel på kjøring i deloppgavene
print(is_vocal('c'))
print(is_vocal('e'))
print(number_of_vocals('Winter is coming.'))
print(number_of_consonants('Winter is coming.'))
