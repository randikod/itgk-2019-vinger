#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# oppgave a
def get_chapters(filename, chapter_delimiter):
    f = open(filename)
    text = f.read()
    f.close()
    
    chapters = text.split(chapter_delimiter)
    del chapters[0]
    return chapters


# oppgave b
def count_words(string_list, word):
    word = word.lower()
    word_counts = []
    for string in string_list:
        word_counts.append(string.lower().count(word))
    return word_counts


# oppgave c
def create_numbers_to(number):
    return list(range(1, number+1))


# oppgave d
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')

def analyze_book(filename, chapter_delimiter, word):
    chapters = get_chapters(filename, chapter_delimiter)
    word_counts = count_words(chapters, word)
    chapter_numbers = create_numbers_to(len(word_counts))
    
    plt.plot(chapter_numbers, word_counts)
    plt.title('Antall ganger "' + word + '" dukker opp per kapittel i "' + filename + '"')
    plt.xlabel('Kapittel')
    plt.ylabel('Antall "' + word + '"')
    plt.axis([1, len(chapters), 0, max(word_counts) + 3])
    plt.show()


# oppgave e
def analyze_words(filename, chapter_delimiter, words):
    for word in words:
        analyze_book(filename, chapter_delimiter, word)

        
# testkode til oppgave e (denne er oppgitt)
words = ['rabbit', 'caterpillar', 'sister', 'cat', 'queen', 'turtle', 'hatter']
analyze_words('alice_in_wonderland.txt', 'CHAPTER', words)


# eksempel oppgave f
words = ['peter', 'peter pan', 'captain', 'crocodile', 'noodler', 'Tinker Bell', 'Queen Mab']
analyze_words('peter_pan.txt', 'Chapter', words)

